-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 22, 2020 at 03:22 PM
-- Server version: 10.4.13-MariaDB
-- PHP Version: 7.4.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `unep_projects_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `completion_status`
--

CREATE TABLE `completion_status` (
  `id` int(11) NOT NULL,
  `description` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `completion_status`
--

INSERT INTO `completion_status` (`id`, `description`) VALUES
(1, 'completed'),
(2, 'under implementation');

-- --------------------------------------------------------

--
-- Table structure for table `countries`
--

CREATE TABLE `countries` (
  `id` int(11) NOT NULL,
  `country_name` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `implementing_offices`
--

CREATE TABLE `implementing_offices` (
  `id` bigint(20) NOT NULL,
  `office_name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `implementing_offices`
--

INSERT INTO `implementing_offices` (`id`, `office_name`) VALUES
(1, 'Europe Office'),
(2, 'Economy Division');

-- --------------------------------------------------------

--
-- Table structure for table `projects`
--

CREATE TABLE `projects` (
  `id` bigint(20) NOT NULL,
  `project_ref` varchar(100) NOT NULL,
  `title` varchar(255) NOT NULL,
  `start_date` date NOT NULL,
  `end_date` date NOT NULL,
  `date_from_gcf` date NOT NULL,
  `grant_amount` decimal(10,2) NOT NULL,
  `first_disbursement_amount` decimal(10,2) NOT NULL,
  `readiness_type_id` int(11) NOT NULL,
  `implementing_office_id` bigint(20) NOT NULL,
  `completion_status_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `projects`
--

INSERT INTO `projects` (`id`, `project_ref`, `title`, `start_date`, `end_date`, `date_from_gcf`, `grant_amount`, `first_disbursement_amount`, `readiness_type_id`, `implementing_office_id`, `completion_status_id`) VALUES
(4, '8977H', 'UYTRFV', '2020-11-04', '2020-11-17', '2020-11-27', '6000.00', '8000.00', 1, 2, 2),
(9, '9uouhhi', 'fyfguvuv', '2020-11-02', '2020-11-19', '2020-11-21', '7898.00', '57888.00', 1, 1, 2),
(10, '7667688', '7thuy88', '2020-11-10', '2020-11-12', '2020-11-21', '6000.00', '7000.00', 2, 1, 2);

-- --------------------------------------------------------

--
-- Table structure for table `readiness_types`
--

CREATE TABLE `readiness_types` (
  `id` int(11) NOT NULL,
  `description` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `readiness_types`
--

INSERT INTO `readiness_types` (`id`, `description`) VALUES
(1, 'Capacity Building'),
(2, 'FI/TNA/other');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `completion_status`
--
ALTER TABLE `completion_status`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `countries`
--
ALTER TABLE `countries`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `implementing_offices`
--
ALTER TABLE `implementing_offices`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `projects`
--
ALTER TABLE `projects`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `project_ref_idx` (`project_ref`),
  ADD KEY `implementing_office_id_fk` (`implementing_office_id`),
  ADD KEY `completion_status_id_fk` (`completion_status_id`),
  ADD KEY `readiness_type_id_fk` (`readiness_type_id`);

--
-- Indexes for table `readiness_types`
--
ALTER TABLE `readiness_types`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `completion_status`
--
ALTER TABLE `completion_status`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `countries`
--
ALTER TABLE `countries`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `implementing_offices`
--
ALTER TABLE `implementing_offices`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `projects`
--
ALTER TABLE `projects`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `readiness_types`
--
ALTER TABLE `readiness_types`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `projects`
--
ALTER TABLE `projects`
  ADD CONSTRAINT `completion_status_id_fk` FOREIGN KEY (`completion_status_id`) REFERENCES `completion_status` (`id`),
  ADD CONSTRAINT `implementing_office_id_fk` FOREIGN KEY (`implementing_office_id`) REFERENCES `implementing_offices` (`id`),
  ADD CONSTRAINT `readiness_type_id_fk` FOREIGN KEY (`readiness_type_id`) REFERENCES `readiness_types` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
