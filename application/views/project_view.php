<div class="outer-container">


	<div class="container-sm col-sm-12">

		<br>
		<h3 class="card-header text-center font-weight-bold text-uppercase py-4">Projects</h3>
		<button type="button" data-toggle="modal" data-target="#add_data_modal" class="btn btn-warning btn-sm">New</button>
		<br>

		<table id="tblProjects" class="table table-bordered">
			<thead>
				<tr>
					<th style="display: none;">Id</th>
					<th>Project Ref</th>
					<th>Project</th>
					<th style="display: none;">Start Date</th>
					<th style="display: none;">End Date</th>
					<th style="display: none;">Date From GCF</th>
					<th>Readiness Type</th>
					<th style="display: none;">Readiness Type Id</th>
					<th>Implementing Office</th>
					<th style="display: none;">Implementing Office Id</th>
					<th style="display: none;">Grant Amount</th>
					<th style="display: none;">First Disbursement Amount</th>
					<th>Status</th>
					<th style="display: none;">Completion Status Id</th>
					<th></th>
				</tr>
			</thead>
			<tbody>
				<?php if ($projects) : ?>
					<?php foreach ($projects as $p) : ?>
						<tr>
							<td style="display: none;"><?php echo $p->id; ?></td>
							<td><?php echo $p->project_ref; ?></td>
							<td><?php echo $p->title; ?></td>
							<td style="display: none;"><?php echo $p->start_date; ?></td>
							<td style="display: none;"><?php echo $p->end_date; ?></td>
							<td style="display: none;"><?php echo $p->date_from_gcf; ?></td>
							<td><?php echo $p->readiness_type; ?></td>
							<td style="display: none;"><?php echo $p->readiness_type_id; ?></td>
							<td><?php echo $p->implementing_office; ?></td>
							<td style="display: none;"><?php echo $p->implementing_office_id; ?></td>
							<td style="display: none;"><?php echo $p->grant_amount; ?></td>
							<td style="display: none;"><?php echo $p->first_disbursement_amount; ?></td>
							<td><?php echo $p->completion_status; ?></td>
							<td style="display: none;"><?php echo $p->completion_status_id; ?></td>
							<td>
								<span><button type="button" class="btn btn-primary btn-sm btnEdit">Edit</button></span>
								<span><button type="button" class="btn btn-danger btn-sm btnDelete">Delete</button></span>
								<span><button type="button" class="btn btn-info btn-sm btnDetails">Details</button></span>
							</td>
						</tr>
					<?php endforeach; ?>
				<?php endif; ?>
			</tbody>
		</table>
	</div>
</div>
<!-- Add -->
<div id="add_data_modal" class="modal fade">
	<div class="modal-dialog">
		<div class="modal-content">
			<form action="<?php echo base_url('index.php/ProjectsController/create') ?>" method="POST">
				<div class="modal-header">
					<h5 class="modal-title" id="modalCreateTitle">Create Project</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<div class="col-md-12">
							<div class="form-group">
								<strong>Project Ref</strong>
								<input type="text" name="project_ref" class="form-control form-control-sm" placeholder="Project Ref" required>
							</div>
							<div class="form-group">
								<strong>Project Title</strong>
								<input type="text" name="project_title" class="form-control form-control-sm" placeholder="Project Title" required>
							</div>
							<div class="form-group">
								<strong>Start Date</strong>
								<input type="date" name="start_date" class="form-control form-control-sm" required>
							</div>
							<div class="form-group">
								<strong>End Date</strong>
								<input type="date" name="end_date" class="form-control form-control-sm" required>
							</div>
							<div class="form-group">
								<strong>Date From GCF</strong>
								<input type="date" name="date_from_gcf" class="form-control form-control-sm" required>
							</div>
						
							<div class="form-group">
								<strong>First Disbursement Amount</strong>
								<input type="number" name="first_disbursement_amount" class="form-control form-control-sm" required>
							</div>
							<div class="form-group">
								<strong>Grant Amount</strong>
								<input type="number" name="grant_amount" class="form-control form-control-sm" required>
							</div>
							<div class="form-group">
								<strong>Readiness Type</strong>
								<select class="form-control form-control-sm" name="readiness_type_id" required>
									<option value="">Select</option>
									<?php foreach ($readiness_types as $type) : ?>
										<option value="<?php echo $type->id; ?>"><?php echo $type->description; ?></option>
									<?php endforeach; ?>
								</select>
							</div>
							<div class="form-group">
								<strong>Implementing Office</strong>
								<select class="form-control form-control-sm" name="implementing_office_id" required>
									<option value="">Select</option>
									<?php foreach ($implementing_offices as $office) : ?>
										<option value="<?php echo $office->id; ?>"><?php echo $office->office_name; ?></option>
									<?php endforeach; ?>
								</select>
							</div>
							<div class="form-group">
								<strong>Completion Status</strong>
								<select class="form-control form-control-sm" name="completion_status_id" required>
									<option value="">Select</option>
									<?php foreach ($completion_status as $status) : ?>
										<option value="<?php echo $status->id; ?>"><?php echo $status->description; ?></option>
									<?php endforeach; ?>
								</select>
							</div>
					
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
					<button type="submit" class="btn btn-primary">Save changes</button>
				</div>
			</form>
		</div>

	</div>
</div>
<!-- Edit -->
<div id="edit_data_modal" class="modal fade" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<form action="<?php echo base_url('index.php/ProjectsController/update') ?>" method="POST">
				<div class="modal-header">
					<h5 class="modal-title" id="exampleModalLabel">Update Project</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body col-md-12">
					<input type="hidden" name="id" id="id">
					<div class="row col-md-12">
						<div class="form-group">
							<strong>Project Ref</strong>
							<input type="text" name="project_ref" id="project_ref" class="form-control form-control-sm" placeholder="Project Title" required>
						</div>
						<div class="form-group">
							<strong>Project Title</strong>
							<input type="text" name="project_title" id="project_title" class="form-control form-control-sm" placeholder="Project Title" required>
						</div>
						<div class="form-group">
							<strong>Start Date</strong>
							<input type="date" name="start_date" id="start_date" class="form-control form-control-sm" required>
						</div>
						<div class="form-group">
							<strong>End Date</strong>
							<input type="date" name="end_date" id="end_date" class="form-control form-control-sm" required>
						</div>
						<div class="form-group">
							<strong>Date From GCF</strong>
							<input type="date" name="date_from_gcf" id="date_from_gcf" class="form-control form-control-sm" required>
						</div>
						<div class="form-group">
							<strong>First Disbursement Amount</strong>
							<input type="number" name="first_disbursement_amount" id="first_disbursement_amount" class="form-control form-control-sm" required>
						</div>
						<div class="form-group">
							<strong>Grant Amount</strong>
							<input type="number" name="grant_amount" id="grant_amount" class="form-control form-control-sm" required>
						</div>
						<div class="form-group">
							<strong>Readiness Type</strong>
							<select class="form-control form-control-sm" name="readiness_type_id" id="readiness_type_id" required>
								<?php foreach ($readiness_types as $readiness) : ?>
									<option value="<?php echo $readiness->id; ?>"><?php echo $readiness->description; ?></option>
								<?php endforeach; ?>
							</select>
						</div>
						<div class="form-group">
							<strong>Implementing Office</strong>
							<select class="form-control form-control-sm" name="implementing_office_id" id="implementing_office_id" required>
								<?php foreach ($implementing_offices as $office) : ?>
									<option value="<?php echo $office->id; ?>"><?php echo $office->office_name; ?></option>
								<?php endforeach; ?>
							</select>
						</div>
						<div class="form-group">
							<strong>Status</strong>
							<select class="form-control form-control-sm" name="completion_status_id" id="completion_status_id" required>
								<?php foreach ($completion_status as $status) : ?>
									<option value="<?php echo $status->id; ?>"><?php echo $status->description; ?></option>
								<?php endforeach; ?>
							</select>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
					<button type="submit" class="btn btn-primary">Save changes</button>
				</div>
			</form>
		</div>

	</div>
</div>

<!-- Delete -->
<div id="delete_data_modal" class="modal fade" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<form action="<?php echo base_url('index.php/ProjectsController/delete/' . $p->id) ?>" method="post">
				<div class="modal-header">
					<h3 class="modal-title" id="exampleModalLabel">Delete</h3>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<span>Are you sure you want to delete this?</span>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
					<button type="submit" class="btn btn-danger">Delete</button>
				</div>
			</form>
		</div>
	</div>
</div>

<!-- Details -->
<div id="details_data_modal" class="modal fade">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">Project Details</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body col-md-12">
				<input type="hidden" name="id" id="id">
				<div class="row col-md-12">
					<div class="form-group">
						<strong>Project Ref</strong>
						<label id="lbl_project_ref" class="form-control form-control-sm"></label>
					</div>
					<div class="form-group">
						<strong>Project Title</strong>
						<label id="lbl_project_title" class="form-control form-control-sm"></label>
					</div>
					<div class="form-group">
						<strong>Start Date</strong>
						<label id="lbl_start_date" class="form-control form-control-sm"></label>
					</div>
					<div class="form-group">
						<strong>End Date</strong>
						<label id="lbl_end_date" class="form-control form-control-sm"></label>
					</div>
					<div class="form-group">
						<strong>Date From GCF</strong>
						<label id="lbl_date_from_gcf" class="form-control form-control-sm"></label>
					</div>
					<div class="form-group">
						<strong>First Disbursement Amount</strong>
						<label id="lbl_first_disbursement_amount" class="form-control form-control-sm"></label>
					</div>
					<div class="form-group">
						<strong>Grant Amount</strong>
						<label id="lbl_grant_amount" class="form-control form-control-sm"></label>
					</div>
					<div class="form-group">
						<strong>Readiness Type</strong>
						<label id="lbl_readiness_type" class="form-control form-control-sm"></label>
					</div>
					<div class="form-group">
						<strong>Implementing Office</strong>
						<label id="lbl_implementing_office" class="form-control form-control-sm"></label>
					</div>
					<div class="form-group">
						<strong>Status</strong>
						<label id="lbl_status" class="form-control form-control-sm"></label>
					</div>
				</div>

			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>

	<script type="text/javascript">
		$(document).ready(function() {
			$('#tblProjects').DataTable({

			});

			$('.btnEdit').click(function() {
				$('#edit_data_modal').modal('show');

				var $tr = $(this).closest('tr');
				var data = $tr.children("td").map(function() {
					return $(this).text();
				}).get();

				$('#id').val(data[0]);
				$('#project_ref').val(data[1]);
				$('#project_title').val(data[2]);
				$('#start_date').val(data[3]);
				$('#end_date').val(data[4]);
				$('#date_from_gcf').val(data[5]);
				$('#readiness_type_id').val(data[7]);
				$('#implementing_office_id').val(data[9]);
				$('#grant_amount').val(data[10]);
				$('#first_disbursement_amount').val(data[11]);
				$('#completion_status_id').val(data[13]);
			});

			$('.btnDelete').click(function() {
				$('#delete_data_modal').modal('show');
			});

			$('.btnDetails').click(function() {
				$('#details_data_modal').modal('show');

				var $tr = $(this).closest('tr');
				var data = $tr.children("td").map(function() {
					return $(this).text();
				}).get();

				$('#id').val(data[0]);
				$('#lbl_project_ref').text(data[1]);
				$('#lbl_project_title').text(data[2]);
				$('#lbl_start_date').text(data[3]);
				$('#lbl_end_date').text(data[4]);
				$('#lbl_date_from_gcf').text(data[5]);
				$('#lbl_readiness_type').text(data[6]);
				$('#lbl_implementing_office').text(data[8]);
				$('#lbl_grant_amount').text(data[10]);
				$('#lbl_first_disbursement_amount').text(data[11]);
				$('#lbl_status').text(data[12]);
			});
		});
	</script>