<?php
class ProjectsController extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->load->model('ProjectModel');

		$this->load->helper('url_helper');
		$this->load->helper('form');
		$this->load->library('form_validation');
	}

	public function index()
	{
		$data['projects'] = $this->ProjectModel->all_projects();
		$data['implementing_offices'] = $this->ProjectModel->all_implementing_offices();
		$data['readiness_types'] = $this->ProjectModel->all_readiness_types();
		$data['completion_status'] = $this->ProjectModel->all_completion_status();
		$this->load->view('header');
		$this->load->view('project_view', $data);
		$this->load->view('footer');
	}

	public function details($id)
	{
		$data['details'] = $this->ProjectModel->project_details($id);

		$this->load->view('header');
		$this->load->view('details', $data);
		$this->load->view('footer');
	}

	public function create()
	{
		$data = array(
			'project_ref' => $this->input->post('project_ref'),
			'title' => $this->input->post('project_title'),
			'start_date' => $this->input->post('start_date'),
			'end_date' => $this->input->post('end_date'),
			'date_from_gcf' => $this->input->post('date_from_gcf'),
			'grant_amount' => $this->input->post('grant_amount'),
			'first_disbursement_amount' => $this->input->post('first_disbursement_amount'),
			'readiness_type_id' => $this->input->post('readiness_type_id'),
			'completion_status_id' => $this->input->post('completion_status_id'),
			'implementing_office_id' => $this->input->post('implementing_office_id')
		 );
		
		$this->ProjectModel->create_project($data);
	
	}

	public function update()
	{
		$id = $this->input->post('id');

		$data = array(
			'project_ref' => $this->input->post('project_ref'),
			'title' => $this->input->post('project_title'),
			'start_date' => $this->input->post('start_date'),
			'end_date' => $this->input->post('end_date'),
			'date_from_gcf' => $this->input->post('date_from_gcf'),
			'grant_amount' => $this->input->post('grant_amount'),
			'first_disbursement_amount' => $this->input->post('first_disbursement_amount'),
			'readiness_type_id' => $this->input->post('readiness_type_id'),
			'completion_status_id' => $this->input->post('completion_status_id'),
			'implementing_office_id' => $this->input->post('implementing_office_id')
		 );
	    $this->ProjectModel->update_project($id,$data);
	
	}
	
	public function delete($id)
	{
		$this->ProjectModel->delete_project($id);
		
	}
}
