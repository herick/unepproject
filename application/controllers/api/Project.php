
<?php

require APPPATH . 'libraries/REST_Controller.php';

class Project extends REST_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('ProjectModel');
    }

    public function index_get($id = 0)
	{
        if(!empty($id)){
            $data = $this->ProjectModel->find_project($id);
        }
        else{
            $data = $this->ProjectModel->all_projects();
        }
     
        $this->response($data, REST_Controller::HTTP_OK);
    }
    
    public function index_get_by_country($country)
    {
        $data = $this->ProjectModel->find_project_by_country($country);
        $this->response($data, REST_Controller::HTTP_OK);
    }
     
    public function index_get_by_status($status)
    {
        $data = $this->ProjectModel->find_project_by_status($status);
        $this->response($data, REST_Controller::HTTP_OK);
    }
    public function index_post()
    {
        $input = $this->input->post();
        $this->ProjectModel->create_project($input);
        $this->response(['Project created successfully.'], REST_Controller::HTTP_OK);
    } 
     
    
    public function index_put($id)
    {
        $input = $this->put();
        $this->ProjectModel->update_project($id,$input);
     
        $this->response(['Project updated successfully.'], REST_Controller::HTTP_OK);
    }
     
   
    public function index_delete($id)
    {
        $this->ProjectModel->delete_project($id);
       
        $this->response(['Project deleted successfully.'], REST_Controller::HTTP_OK);
    }
}
