<?php
class ProjectModel extends CI_Model
{

    public function __construct()
    {
        $this->load->database();
    }

    public function all_projects()
    {
        $query = $this->db->get('projects');

        $this->db->select('projects.id,projects.project_ref,projects.title,projects.start_date,projects.end_date,projects.date_from_gcf,
        grant_amount,first_disbursement_amount,projects.readiness_type_id,readiness_types.description as readiness_type,projects.completion_status_id,
        completion_status.description as completion_status,projects.implementing_office_id, implementing_offices.office_name as implementing_office');
        $this->db->from('projects');
        $this->db->join('completion_status', 'completion_status.id = projects.completion_status_id');
        $this->db->join('readiness_types', 'readiness_types.id = projects.readiness_type_id');
        $this->db->join('implementing_offices', 'implementing_offices.id = projects.implementing_office_id');
      
        $query = $this->db->get();

        return $query->result();
    }

    public function all_readiness_types()
    {
        $query = $this->db->get('readiness_types');
        return $query->result();
    }

    public function all_completion_status()
    {
        $query = $this->db->get('completion_status');
        return $query->result();
    }

    public function all_implementing_offices()
    {
        $query = $this->db->get('implementing_offices');
        return $query->result();
    }

    public function project_details($id)
    {
        $query = $this->db->query('');
        return $query->result();
    }

    public function find_project_by_status($status)
    {
        $this->db->select('*');
        $this->db->from('projects');
        $this->db->join('completion_status', 'completion_status.id = projects.completion_status_id');
        $this->db->join('readiness_types', 'readiness_types.id = projects.readiness_type_id');
        $this->db->get_where('completion_status', array('description' => $status));
        $query = $this->db->get();
        return $query->result();
    }

    public function find_project($id){
        $this->db->select('*');
        $this->db->from('projects');
        $this->db->get_where('id',$id);
        $query = $this->db->get();
        return $query->row();
    }

    public function find_project_by_country($country){
        $this->db->select('*');
        $this->db->from('projects');
        $this->db->join('countries', 'projects.country_id = countries.id');
        $this->db->get_where('countries.country_name',$country);
        $query = $this->db->get();
        return $query->result();
    }

    public function create_project($data)
    {
        $this->db->insert('projects', $data);
    }

    public function update_project($id, $data)
    {
        $this->db->where("id", $id);
        $this->db->update("projects", $data);
    }


    public function delete_project($id)
    {
        $this->db->where('id', $id);
        return $this->db->delete('projects');
    }
}
